<?php
/**
 * WordPress基础配置文件。
 *
 * 这个文件被安装程序用于自动生成wp-config.php配置文件，
 * 您可以不使用网站，您需要手动复制这个文件，
 * 并重命名为“wp-config.php”，然后填入相关信息。
 *
 * 本文件包含以下配置选项：
 *
 * * MySQL设置
 * * 密钥
 * * 数据库表名前缀
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/zh-cn:%E7%BC%96%E8%BE%91_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL 设置 - 具体信息来自您正在使用的主机 ** //
/** WordPress数据库的名称 */
define('WP_CACHE', true);
define( 'WPCACHEHOME', '/www/gnt.lvovl.cn/wp-content/plugins/wp-super-cache/' );
define('DB_NAME', 'gnt_lvovl_cn');

/** MySQL数据库用户名 */
define('DB_USER', 'gnt');

/** MySQL数据库密码 */
define('DB_PASSWORD', 'rG198126~');

/** MySQL主机 */
define('DB_HOST', 'gnt.lvovl.cn');

/** 创建数据表时默认的文字编码 */
define('DB_CHARSET', 'utf8mb4');

/** 数据库整理类型。如不确定请勿更改 */
define('DB_COLLATE', '');

/**#@+
 * 身份认证密钥与盐。
 *
 * 修改为任意独一无二的字串！
 * 或者直接访问{@link https://api.wordpress.org/secret-key/1.1/salt/
 * WordPress.org密钥生成服务}
 * 任何修改都会导致所有cookies失效，所有用户将必须重新登录。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[ewt[$ ?C%xdFYgp8?nE(`A4SC8iy8a.;]_M<V`4/&7Rl*DPRY%wN;DDW0SHBwuv');
define('SECURE_AUTH_KEY',  't)U*BMx%s=KMj1L)+~~N:T=1+sG:o;Rs&./(244Ig(Hs~&C3L2oSM5D;x*+aO5e=');
define('LOGGED_IN_KEY',    'gGBg^^[HQEVn6z<uXor7L8Im1&YNKs3tM -6rOa1fc| u|0;DL2X o&d/PcX1~CK');
define('NONCE_KEY',        '%`6rV,]mdJQ0a+IX|b3C:dA02dqVXl1d(8^R8#RyY^btuM]n`9W=5+V)(y6<f{x?');
define('AUTH_SALT',        'sJ<jpPcF4g{o*&F97s PO2?5$k4Ec2QP^&9ebDa[_NAY#~fKckty, Gs(cW:;}FE');
define('SECURE_AUTH_SALT', 'pET.j~),UgzrajQ2{DND9G]K.usS<niC~cCI]phb_va#4H}?jA[-}29~G^2NvmIo');
define('LOGGED_IN_SALT',   '!!-}CG`o#)nyM M3Z<+/0dOlE!7zba4]DOj<NKI15-nCt&lk5%c.LA|4Eew,K$v3');
define('NONCE_SALT',       '`g._ /sw}d N2$s=iAI0s{WQa>6=r*/P*JA?Ivx?T@Q|VSSI59jvhjab]yaPS]iP');

/**#@-*/

/**
 * WordPress数据表前缀。
 *
 * 如果您有在同一数据库内安装多个WordPress的需求，请为每个WordPress设置
 * 不同的数据表前缀。前缀名只能为数字、字母加下划线。
 */
$table_prefix  = 'gnt_';

/**
 * 开发者专用：WordPress调试模式。
 *
 * 将这个值改为true，WordPress将显示所有用于开发的提示。
 * 强烈建议插件开发者在开发环境中启用WP_DEBUG。
 *
 * 要获取其他能用于调试的信息，请访问Codex。
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/**
 * zh_CN本地化设置：启用ICP备案号显示
 *
 * 可在设置→常规中修改。
 * 如需禁用，请移除或注释掉本行。
 */
define('WP_ZH_CN_ICP_NUM', true);

/* 好了！请不要再继续编辑。请保存本文件。使用愉快！ */

/** WordPress目录的绝对路径。 */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** 设置WordPress变量和包含文件。 */
require_once(ABSPATH . 'wp-settings.php');
