<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<span class="sku_wrapper"><?php _e( 'SKU:', 'woocommerce' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?></span>.</span>

	<?php endif; ?>
	
	<?php 
	
		global $woocommerce;
	
		if ( version_compare( $woocommerce->version, '3.0.0', '>=' ) ) {
			
			echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( '<span class="posted_in_cat">Category:</span>', '<span class="posted_in_cat">Categories:</span>', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '.</span>' );
			echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="tagged_as">' . _n( '<span class="posted_in_cat">Tag:</span>', '<span class="posted_in_cat">Tags:</span>', count( $product->get_tag_ids() ), 'woocommerce'  ) . ' ', '.</span>' );

		} else {
			
			global $post;
			
			$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
			$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );
			
			echo $product->get_categories( ', ', '<span class="posted_in">' . _n( '<span class="posted_in_cat">Category:</span>', '<span class="posted_in_cat">Categories:</span>', $cat_count, 'woocommerce' ) . ' ', '.</span>' ); 
			echo $product->get_tags( ', ', '<span class="tagged_as">' . _n( '<span class="posted_in_cat">Tag:</span>', '<span class="posted_in_cat">Tags:</span>', $tag_count, 'woocommerce' ) . ' ', '.</span>' );
			
		}
	
	?>

	<?php  ?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>