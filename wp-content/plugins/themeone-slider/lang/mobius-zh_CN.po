msgid ""
msgstr ""
"Project-Id-Version: Themeone Slider\n"
"POT-Creation-Date: 2014-10-01 20:55+0100\n"
"PO-Revision-Date: 2017-09-09 13:49+0800\n"
"Last-Translator: \n"
"Language-Team: Themeone <themeone.master@gmail.com>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.1\n"
"X-Poedit-KeywordsList: __;_e;_x\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../themeone-slider.php:33 ../themeone-slider.php:139
msgid "Slides"
msgstr "幻灯片"

#: ../themeone-slider.php:34
msgid "Slide"
msgstr "幻灯"

#: ../themeone-slider.php:35
msgid "Search Slides"
msgstr "搜索幻灯片"

#: ../themeone-slider.php:36
msgid "All Slides"
msgstr "所有幻灯片"

#: ../themeone-slider.php:37
msgid "Parent Slide"
msgstr "父级幻灯片"

#: ../themeone-slider.php:38
msgid "Edit Slide"
msgstr "编辑幻灯片"

#: ../themeone-slider.php:39
msgid "Update Slide"
msgstr "更新幻灯片"

#: ../themeone-slider.php:40
msgid "Add New Slide"
msgstr "添加幻灯片"

#: ../themeone-slider.php:41 ../themeone-slider.php:46
msgid "Themeone Slider"
msgstr "Themeone 幻灯片"

#: ../themeone-slider.php:68
msgid "Sliders"
msgstr "幻灯轮播"

#: ../themeone-slider.php:69 ../themeone-slider.php:76
#: ../themeone-slider.php:81 ../themeone-slider.php:138
msgid "Slider"
msgstr "幻灯轮播"

#: ../themeone-slider.php:70
msgid "Search Sliders"
msgstr "搜索幻灯轮播"

#: ../themeone-slider.php:71
msgid "All Sliders"
msgstr "所有幻灯轮播"

#: ../themeone-slider.php:72
msgid "Parent Slider"
msgstr "父级幻灯轮播"

#: ../themeone-slider.php:73
msgid "Edit Slider"
msgstr "编辑幻灯轮播"

#: ../themeone-slider.php:74
msgid "Update Slider"
msgstr "更新幻灯轮播"

#: ../themeone-slider.php:75
msgid "Add Slider"
msgstr "添加幻灯轮播"

#: ../themeone-slider.php:99
msgid "Slide Image"
msgstr "幻灯片图像"

#: ../themeone-slider.php:100
msgid "Name"
msgstr "名称"

#: ../themeone-slider.php:101
msgid "Alignment"
msgstr "对齐"

#: ../themeone-slider.php:102
msgid "Video"
msgstr "视频"

#: ../themeone-slider.php:103 ../themeone-slider.php:140
msgid "Date"
msgstr "日期"
